#include <algorithm>
#include <iostream>
#include <array>

using namespace std;

int main() {

    int cars[] = {2, 18, 7, 6, 19, 23}; 
    int n = sizeof(cars)/sizeof(cars[0]);
    int k = 4;
    
    sort(cars, cars+n);
    
    float min_dis = 1e14;
    
    for (int i = k - 1; i < n; i++) {
        min_dis = min(float(cars[i] - cars[i - k + 1]), min_dis);
    }
    cout << min_dis + 1 << " ";
    
    return 0;
    
}



